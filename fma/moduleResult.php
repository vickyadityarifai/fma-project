<?php

session_start();
// do check
if (!isset($_SESSION["firstname"])) {
//if (!isset($_SESSION["firstname"]) || !isset($_SESSION["surname"])) {
    header("location: index.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birkbeck College : VARIABLE different for selected view</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/styles.css?" />
</head>
<body>
	<header>
		<img src="images/birkbeckLogo.jpg" alt="Birkbeck College"style="float:left">	
        <h1>Web Programming Using PHP - FMA</h1>
		<nav>
			<!-- NAV menu -->
			<ul>
				<li><a href="home.php">Home Page</a></li>
				<li><a href="moduleResult.php" title="Available to academic and admin users after login.">Module Results</a></li>
				<?php

				if ($_SESSION['userType'] == 'admin'){

					echo "<li><a href='users.php' title='Available to admin users only. Includes Create/Read/Update/Delete options after login'>User Administration</a></li>";
				} else {
					echo "";
				}
				
				?>
				<li><?php echo "Hi, ".$_SESSION['firstname']." ".$_SESSION['surname']; ?></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</nav>
	</header>
	<main>
		<!-- Selected View Heading -->
		<h2>Module Results</h2>
		<!-- Selected View content, as per the Functional Specification -->
		<h3>Module Title : p1 : Web Programming Using PHP</h3>
		<table>
			<th>
				Statistic
			</th>
			<th>
				Number	
			</th>
				<?php
				$sql = mysqli_query($koneksi, "
					SELECT (CASE WHEN moduleResult >= 70 THEN '1st'
					            WHEN moduleResult >= 60 THEN '2.1'
					            WHEN moduleResult >= 50 THEN '2.2'
					            WHEN moduleResult >= 45 THEN '3rd'
					            WHEN moduleResult >= 40 THEN 'Pass'
								ELSE 'Fail'
						   END) AS Statistic, count(CASE WHEN moduleResult >= 70 THEN '1st'
					            WHEN moduleResult >= 60 THEN '2.1'
					            WHEN moduleResult >= 50 THEN '2.2'
					            WHEN moduleResult >= 45 THEN '3rd'
					            WHEN moduleResult >= 40 THEN 'Pass'
								ELSE 'Fail'
						   END) as Number
					FROM moduleResult
					WHERE moduleCode = 'p1'
					GROUP BY Statistic
				");

				while($c = mysqli_fetch_array($sql)) {
 							
				?>			
			<tr>
				<td><?php echo $c['Statistic']; ?></td>
				<td><?php echo $c['Number']; ?></td>
			</tr>
			<?php
			}
			?>
			<tr>
				<td>Average Mark</td>
				<td>
					<?php
					$sql = mysqli_query($koneksi, "SELECT ROUND(AVG(moduleResult),0) as averageMark 
					FROM moduleResult
					WHERE moduleCode = 'p1'");

					$d = mysqli_fetch_array($sql);

					echo $d['averageMark'];
					?>					
				</td>
			</tr>
			<tr>
				<td>Total Student</td>
				<td>
					<?php
					$sql = mysqli_query($koneksi, "SELECT COUNT(moduleResult) as countMarks FROM moduleResult WHERE moduleCode = 'p1'");
					
					$a = mysqli_fetch_array($sql);

					echo $a['countMarks'];
					?>
				</td>
			</tr>
		</table>
	</main> 
	<footer>
		<p>Department of Computer Science and Information Systems</p>
		<p>Birkbeck, University of London</p>
		<p>Malet Street</p>
		<p>London WC1E 7HX</p>
	</footer>
</body>
</html>

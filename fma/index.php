<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birkbeck College : VARIABLE different for selected view</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/styles.css?" />
</head>
<body>
	<header>
		<img src="images/birkbeckLogo.jpg" alt="Birkbeck College"style="float:left">	
        <h1>Web Programming Using PHP - FMA</h1>
		<form method="post" action="login_success.php">
			<fieldset>
			<legend>Login</legend>
			<div>
				<label for="un">Username:</label>
				<input type="text" name="userName" id="un"/>
				<label for="pwd">Password:</label>
				<input type="password" name="password" id="pwd"/>
				<input type="submit" name="submit" value="login" />
			</div>
			</fieldset>
		</form>
		<nav>
			<!-- NAV menu -->
			<ul>
				<li><a href="index.php">Home Page</a></li>
<!--				<li><a href=" " title="Available to academic and admin users after login.">Module Results</a></li>
				<li><a href=" " title="Available to admin users only. Includes Create/Read/Update/Delete options after login">User Administration</a></li>
-->			</ul>
	</nav>
	</header>
	<main>
		<!-- Selected View Heading -->
		<h2>Department of Computer Science and Information Systems</h2>
		<!-- Selected View content, as per the Functional Specification -->
		<p>The Department of Computer Science and Information Systems at Birkbeck is one of the first computing departments established in the UK, celebrating our 64th anniversary in 2021. We provide a stimulating teaching and research environment for both part-time and full-time students, and a friendly, inclusive space for learning, working, and collaborating.</p>
		<p>This website is the solution to the Web Programming Using PHP module - Final Marked Assignment.</p>
		<p>You need to successfully login to access module results and/or user administration depending on your user type.</p>
    </main> 
	<footer>
		<p>Department of Computer Science and Information Systems</p>
		<p>Birkbeck, University of London</p>
		<p>Malet Street</p>
		<p>London WC1E 7HX</p>
	</footer>
</body>
</html>

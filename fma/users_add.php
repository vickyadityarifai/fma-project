<?php

session_start();
// do check
if (!isset($_SESSION["firstname"])) {
//if (!isset($_SESSION["firstname"]) || !isset($_SESSION["surname"])) {
    header("location: index.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birkbeck College : VARIABLE different for selected view</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/styles.css?" />
</head>
<body>
	<header>
		<img src="images/birkbeckLogo.jpg" alt="Birkbeck College"style="float:left">	
        <h1>Web Programming Using PHP - FMA</h1>
		<nav>
			<!-- NAV menu -->
			<ul>
				<li><a href="home.php">Home Page</a></li>
				<li><a href="moduleResult.php" title="Available to academic and admin users after login.">Module Results</a></li>
				<?php

				if ($_SESSION['userType'] == 'admin'){

					echo "<li><a href='users.php' title='Available to admin users only. Includes Create/Read/Update/Delete options after login'>User Administration</a></li>";
				} else {
					echo "";
				}
				
				?>
				<li><?php echo "Hi, ".$_SESSION['firstname']." ".$_SESSION['surname']; ?></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</nav>
	</header>
	<main>
		<!-- Selected View Heading -->
		<h2>Add Users |<a href="users.php"> <- Back</a></h2>
		<!-- Selected View content, as per the Functional Specification -->
		<form method="post" action="users_add_save.php">
		<table>
			<tr>
				<td>First Name : </td><td><input type="text" name="firstName"></td>
			</tr>
			<tr>
				<td>Surname : </td><td><input type="text" name="surname"></td>
			</tr>
			<tr>
				<td>Email : </td><td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>Telephone : </td><td><input type="number" name="telephone"></td>
			</tr>
			<tr>
				<td>Username : </td><td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td>Password : </td><td><input type="text" name="password"></td>
			</tr>
			<tr>
				<td>User Type : </td><td>
					<select name="userType">
						<option value="admin">Admin</option>
						<option value="academic">Academic</option>
					</select>
				</td>
			</tr>
		</table>
		<input type="submit" name="submit">
		</form>
    </main> 
	<footer>
		<p>Department of Computer Science and Information Systems</p>
		<p>Birkbeck, University of London</p>
		<p>Malet Street</p>
		<p>London WC1E 7HX</p>
	</footer>
</body>
</html>

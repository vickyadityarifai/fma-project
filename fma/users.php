<?php

session_start();
// do check
if (!isset($_SESSION["firstname"])) {
//if (!isset($_SESSION["firstname"]) || !isset($_SESSION["surname"])) {
    header("location: index.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birkbeck College : VARIABLE different for selected view</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/styles.css?" />
</head>
<body>
	<header>
		<img src="images/birkbeckLogo.jpg" alt="Birkbeck College"style="float:left">	
        <h1>Web Programming Using PHP - FMA</h1>
		<nav>
			<!-- NAV menu -->
			<ul>
				<li><a href="home.php">Home Page</a></li>
				<li><a href="moduleResult.php" title="Available to academic and admin users after login.">Module Results</a></li>
				<?php

				if ($_SESSION['userType'] == 'admin'){

					echo "<li><a href='users.php' title='Available to admin users only. Includes Create/Read/Update/Delete options after login'>User Administration</a></li>";
				} else {
					echo "";
				}
				
				?>
				<li><?php echo "Hi, ".$_SESSION['firstname']." ".$_SESSION['surname']; ?></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</nav>
	</header>
	<main>
		<!-- Selected View Heading -->
		<h2>Users Administration | <a href="users_add.php">+ Add Users Here!!!</a></h2>
		<!-- Selected View content, as per the Functional Specification -->
		<table>
			<th>No</th>
			<th>User Name</th>
			<th>Option</th>
			<?php

			$no=1;
			$sql = mysqli_query($koneksi, "SELECT * from users");
			
			while ($d = mysqli_fetch_array($sql)) {
	
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $d['firstName']." ".$d['surname']; ?></td>
				<td>
					<a href="users_detail.php?id=<?php echo $d['userID']; ?>">Detail - 
					<a href="javascript:deleteData(<?php echo $d['userID']; ?>)"> Delete
				</td>
			</tr>
			<?php 
			}
			?>
		</table>
    </main> 
	<footer>
		<p>Department of Computer Science and Information Systems</p>
		<p>Birkbeck, University of London</p>
		<p>Malet Street</p>
		<p>London WC1E 7HX</p>
	</footer>
     <script language="JavaScript" type="text/javascript">
      function deleteData(id){
        if (confirm("Are You sure want to delete this data?")){
          window.location.href = 'delete.php?id=' + id;
        }
      }
    </script>
</body>
</html>
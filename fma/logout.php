<?php 

session_start();
// do check
if (!isset($_SESSION["firstname"])) {
    header("location: index.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

$username = $_SESSION['username'];

// date_default_timezone_set('Asia/Jakarta');
// $timestamp = date("Y-m-d H:i:s");
// $loginTime = $_SESSION["loginTime"];
// //$query = "insert into userlog VALUES ('','$username',0,'$timestamp')";
// //$query = "UPDATE userlog SET logoutTime = '$timestamp' WHERE( id = max(id) AND username = '$username')";
// $query = "UPDATE userlog SET logoutTime = '$timestamp' WHERE username = '$username' order by id desc limit 1";
// mysqli_query($koneksi, $query);
 
session_destroy();

header("location:index.php");
?>
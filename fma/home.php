<?php

session_start();
// do check
if (!isset($_SESSION["firstname"])) {
//if (!isset($_SESSION["firstname"]) || !isset($_SESSION["surname"])) {
    header("location: index.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birkbeck College : VARIABLE different for selected view</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/styles.css?" />
</head>
<body>
	<header>
		<img src="images/birkbeckLogo.jpg" alt="Birkbeck College"style="float:left">	
        <h1>Web Programming Using PHP - FMA</h1>
		<nav>
			<!-- NAV menu -->
			<ul>
				<li><a href="home.php">Home Page</a></li>
				<li><a href="moduleResult.php" title="Available to academic and admin users after login.">Module Results</a></li>
				<?php

				if ($_SESSION['userType'] == 'admin'){

					echo "<li><a href='users.php' title='Available to admin users only. Includes Create/Read/Update/Delete options after login'>User Administration</a></li>";
				} else {
					echo "";
				}
				
				?>
				<li><?php echo "Hi, ".$_SESSION['firstname']." ".$_SESSION['surname']; ?></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</nav>
	</header>
	<main>
		<!-- Selected View Heading -->
		<h2>Department of Computer Science and Information Systems</h2>
		<!-- Selected View content, as per the Functional Specification -->
		<p>The Department of Computer Science and Information Systems at Birkbeck is one of the first computing departments established in the UK, celebrating our 64th anniversary in 2021. We provide a stimulating teaching and research environment for both part-time and full-time students, and a friendly, inclusive space for learning, working, and collaborating.</p>
		<p>This website is the solution to the Web Programming Using PHP module - Final Marked Assignment.</p>
		<p>You need to successfully login to access module results and/or user administration depending on your user type.</p>
    </main> 
	<footer>
		<p>Department of Computer Science and Information Systems</p>
		<p>Birkbeck, University of London</p>
		<p>Malet Street</p>
		<p>London WC1E 7HX</p>
	</footer>
</body>
</html>

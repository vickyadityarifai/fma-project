-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Sep 2022 pada 02.45
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fmadb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `moduleresult`
--

CREATE TABLE `moduleresult` (
  `moduleCode` varchar(2) NOT NULL,
  `studentID` varchar(8) NOT NULL,
  `moduleResult` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `moduleresult`
--

INSERT INTO `moduleresult` (`moduleCode`, `studentID`, `moduleResult`) VALUES
('dt', '1234567', 51),
('dt', '12345670', 91),
('dt', '1234587', 76),
('dt', '1534569', 42),
('dt', '23456701', 65),
('dt', '34567012', 89),
('dt', '45670123', 61),
('dt', '56701234', 40),
('dt', '67012345', 45),
('dt', '70123456', 82),
('jv', '1234567', 44),
('jv', '12345670', 76),
('jv', '1234587', 66),
('jv', '1534569', 27),
('jv', '23456701', 71),
('jv', '34567012', 77),
('jv', '45670123', 51),
('jv', '56701234', 32),
('jv', '67012345', 45),
('jv', '70123456', 82),
('p1', '1234567', 56),
('p1', '12345670', 76),
('p1', '1234587', 61),
('p1', '1534569', 58),
('p1', '23456701', 45),
('p1', '34567012', 95),
('p1', '45670123', 66),
('p1', '56701234', 21),
('p1', '67012345', 40),
('p1', '70123456', 81);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `userType` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`userID`, `firstName`, `surname`, `email`, `telephone`, `username`, `password`, `userType`) VALUES
(1, 'Admin', 'User', 'adminuser@mail.com', '120230230', 'ubadmin01', 'DCSadmin-01', 'admin'),
(2, 'Ordinary', 'User', 'ordinary@user.com', '1214252112', 'ordinary1', 'Ordi-02', 'academic');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `moduleresult`
--
ALTER TABLE `moduleresult`
  ADD PRIMARY KEY (`moduleCode`,`studentID`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

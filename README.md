Website PDO
Introduction :
1. 2 user type : academic, admin
2. kedua user bisa melihat summary statistic
3. admin user type bisa akses user administrasi CRUD user
4. contoh tampilan bisa dilihat di halaman 1

Functional requirement :
1. initial administrator User bisa login dan regist new user. Username -> ubadmin01, password -> DCSadmin-01, usertype -> admin, firstname -> Admin, surname -> User
2. MODULE RESULT URUSAN SI CLIENT inimah (three .csv files)
3. lihat spesifikasi data untuk ketentuan database.
4. TAMPILAN AWAL SESUAI DENGAN (mockupPage.html and style.css)
5. menu navigasi sesuai dengan usertype.
6. 3 type user dengan hak akses:
	- public (bisa di homepage aja tanpa login)
	- academic (homepage, dan Module Result (setelah login)
	- administrator (homepage, module result, user administration page setelah login)
7. tampilan sukses login dengan nama firstname lalu surname display di semua halaman dan ada logout dan navbar yang sesuai dengan hak akses setelah login
8. logout harus sukses dan kembali menampilkan login akses, dan kembali ke tampilan nav bar menu public
9. COOKIEE
10. Module Result View harus menampilkan sesuai contoh
11. user administration page harus bisa menampilkan atau memperbolehkan akses ke beberapa fungsi administrator 
	- Register/Create new user (first name, surname, email, telephone number, username, dan password, dan user type)
	-Display semua user yang teregistrasi (first name and surname sebagai Username) dengan pilihan
		- select, view/read detail data user yang teregistrasi dengan tampilan tabel / format form gitu.
		- Select and Update data user.
		- select and delete data user dan ada konfirmasi gitu. tidak diperbolehkan delete default admin user atau admin user type yang login ke system juga gabisa delete dirinya sendiri.

Data Specification
1. Module result filenya .csv dan bisa di download oleh clientnya sendiri.
2. module .csv dimasukkan ke dalam database dengan spesifikasi seusi halaman 4
3. users table ada juga spesifikasinya disesuaikan.

Spesifikasi teknis
1. menggunakan MVC
2. akses ke aplikasi hanya melalui login